import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormsModule, NgForm } from '@angular/forms';
import { FormService } from "src/app/Form.service";
import { Form } from '../entity/Form';
import { HttpClientModule } from '@angular/common/http'; 
import { HttpModule } from '@angular/http';


/**
 * @title Stepper overview
 */
@Component({
  selector: 'app-angularmaterial-form',
  templateUrl: './angularmaterial-form.component.html',
  styleUrls: ['./angularmaterial-form.component.css']
})
export class AngularmaterialFormComponent implements OnInit {
  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup; 
  form: FormGroup;

  form2: Form = {
  Collaborateur : null,
  entitytype : null,
  nom: null,
  societe: null,
  email: null,
  telephone: null
  }

  selected:string = null;

  constructor(private _formBuilder: FormBuilder, private formRepo: FormService) { }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
    this.thirdFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
    this.fourthFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
  }

  addForm() {
    // this.formRepo.addForm(this.form).subscribe();
    this.formRepo.addForm(this.form2).subscribe()
  }

}


