import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AngularmaterialFormComponent } from './angularmaterial-form.component';

describe('AngularmaterialFormComponent', () => {
  let component: AngularmaterialFormComponent;
  let fixture: ComponentFixture<AngularmaterialFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AngularmaterialFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AngularmaterialFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
