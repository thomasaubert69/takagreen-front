import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { AppComponent } from '../app.component';
import * as jsPDF from 'jspdf';

@Component({
  selector: 'app-summary-form',
  templateUrl: './summary-form.component.html',
  styleUrls: ['./summary-form.component.css']
})
export class SummaryFormComponent implements OnInit {


  constructor() {
  }

  ngOnInit() {
  }

  @ViewChild('content', null) content: ElementRef;

  public generatePDF() {

    let doc = new jsPDF();
    let specialElementHandlers = {
      '#editor': function (element, renderer) {
        return true;
      }
    };

    let content = this.content.nativeElement;

    doc.fromHTML(content.innerHTML, 15, 15, {
      'width': 190,
      'elementHandlers': specialElementHandlers
    });

    doc.save('test.pdf');

  }




}
