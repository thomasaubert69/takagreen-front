import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap, switchMap } from "rxjs/operators";
import { Observable, BehaviorSubject } from 'rxjs';
import { Form } from 'src/app/entity/Form';
import { HttpClientModule } from '@angular/common/http'; 
import { HttpModule } from '@angular/http';

@Injectable({
    providedIn: 'root'
})
export class FormService {
    private url = 'http://localhost:8000/api/form';
    selected:string = null;
    constructor(private http: HttpClient) { }

    addForm(form: Form) {
        return this.http.post<Form>(this.url, {...form, collaborateur:form.Collaborateur});
    }


    // chopé l'id du post?
    // deletePost(post: Post) {
    //     return this.http.delete<Post>(this.url + '/' + post.id);
    // };


    // Observable+ array des Posts?
    // showPost(): Observable<Post[]> {
    //     return this.http.get<Post[]>(this.url);
    // };

    // singlePost(id: number) {
    //     return this.http.get<Post>(this.url + '/' + id);
    // }

    // modifyPost(post: Post) {
    //     return this.http.patch<Post>(this.url + '/' + post.id, post);
    // }


    // getToken(): string {
    //     return localStorage.getItem('token');
    // }


}
