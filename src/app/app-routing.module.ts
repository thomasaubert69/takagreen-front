import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormComponent } from './form/form.component';
import { MainComponent } from './main/main.component';
import { AngularmaterialFormComponent } from './angularmaterial-form/angularmaterial-form.component';
import { SummaryFormComponent } from './summary-form/summary-form.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { RemerciementComponent } from './remerciement/remerciement.component';


const routes: Routes = [
  { path: '', component: MainComponent },
  { path: 'form', component: FormComponent },
  { path: 'test', component: AngularmaterialFormComponent },
  { path: 'summary', component: SummaryFormComponent },
  { path: 'header', component: HeaderComponent },
  { path: 'footer', component: FooterComponent },
  { path: 'remerciement', component: RemerciementComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
