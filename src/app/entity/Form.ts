export interface Form {
    id?: number,
    Collaborateur: string,
    entitytype: string,
    nom: string,
    societe: string,
    email: string,
    telephone: number
}